// ----- References to Elements -----
// Bank
const bankBalanceElement = document.getElementById("bank-balance");
const loanBankInfoElement = document.getElementById("bank-loan-info");
const loanToPayBalanceElement = document.getElementById("bank-loan-to-pay-balance");
const loanButtonElement = document.getElementById("loan");

// Work
const salaryBalanceElement = document.getElementById("salary-balance");
const transferButtonElement = document.getElementById("transfer");
const repayLoanButtonElement = document.getElementById("work-loan-button");
const earnButtonElement = document.getElementById("earn");

// Laptops
const laptopsSelectElement = document.getElementById("laptops-select");
const laptopsSpecsElement = document.getElementById("laptops-specs");

// Showcase
const showcaseImageElement = document.getElementById("showcase-image").firstElementChild;
const showcaseTitleElement = document.getElementById("showcase-description").firstElementChild;
const showcaseDescriptionElement = document.getElementById("showcase-description").lastElementChild;
const showcasePriceElement = document.getElementById("showcase-price");
const showcaseBuyButtonElement = document.getElementById("showcase-buy-laptop");


// ----- Fields -----
let bankBalance = 0;
let salaryBalance = 0;
const salary = 100;
let hasLoan = false;
let loan = 0;
const loanRatePercent = 0.1;
let availableLoanAmount = 0;
const currencySign = "$";
let laptops = [];
const computersAPI = "https://noroff-komputer-store-api.herokuapp.com/";
const fallbackImageURL = "https://picsum.photos/id/0/300/300";


// ----- BANK, WORK, LAPTOPS functionality -----
function earnMoney() {
    salaryBalance += salary;
}

function transferMoneyToBank() {
    if (salaryBalance <= 0) {
        return;
    }
    let moneyToTransfer = salaryBalance;
    if (hasLoan) {
        const minPayment = salary * loanRatePercent;
        let payment = (minPayment < salaryBalance) ? minPayment : salaryBalance;
        payment = (payment > loan) ? loan : payment;

        loan -= payment;
        moneyToTransfer -= payment;

        if (loan === 0) {
            hasLoan = false;
        }
    }
    bankBalance += moneyToTransfer;
    salaryBalance = 0;
    calculateAvailableLoanAmount();
}

function calculateAvailableLoanAmount() {
    availableLoanAmount = bankBalance * 2;
}

function getLoan() {
    if (hasLoan) {
        alert("Unfortunately, you must pay off your first loan before taking out another loan.");
        return;
    }

    let input = prompt("Hello, human! How much do you want to loan?", "0");
    if (input == null) {
        return;
    }

    let newLoan = Number.parseInt(input);
    if (!Number.isInteger(newLoan)) {
        alert("No deal. Please enter an integer number and try again.");
        return;
    }


    if (newLoan <= 0) {
        alert("No deal. You can't loan zero or below");
        return;
    } else if (bankBalance === 0) {
        alert("Oops. You can't get loan with 0 balance");
        return;
    } else if (newLoan > availableLoanAmount) {
        alert(`No deal. You can't get this much. Please choose amount less or equal ${availableLoanAmount} ${currencySign}`);
        return;
    }

    loan = newLoan;
    bankBalance += newLoan;
    hasLoan = true;
}

function repayLoan() {
    if (salaryBalance <= 0) {
        return;
    }

    let payment = (salaryBalance > loan) ? loan : salaryBalance;
    loan -= payment;
    salaryBalance -= payment;

    if (loan === 0) {
        hasLoan = false;
        calculateAvailableLoanAmount();
    }
}

function buyLaptop(laptop) {
    let laptopPrice = parseInt(laptop.price);
    if (bankBalance < laptopPrice) {
        alert("Unfortunately, you don't have enough money on your bank account to pay for it!");
        return;
    }
    bankBalance -= laptopPrice;
    calculateAvailableLoanAmount();
    alert(`Congratulations! You just bought ${laptop.title} for ${laptop.price} ${currencySign}! Happy using!`)
}

function getFirstLaptop() {
    return laptops[0];
}


// ----- On click functions -----
function onGetLoanClick() {
    getLoan();
    if (hasLoan) {
        renderLoan();
        renderBankBalance();
        renderLoanToPayBalance();
    }
}

function onEarnMoneyClick() {
    earnMoney();
    renderSalaryBalance();
}

function onTransferMoneyToBankClick() {
    transferMoneyToBank();
    renderBankBalance();
    renderLoanToPayBalance();
    renderSalaryBalance();
    if (loan === 0) {
        hideLoan();
    }
}

function onRepayLoanClick() {
    repayLoan();
    renderSalaryBalance();
    renderLoanToPayBalance();
    if (loan === 0) {
        hideLoan();
    }
}

function onBuyLaptopClick() {
    let laptop = laptops.find(laptop => parseInt(laptop.id) === parseInt(laptopsSelectElement.value));
    buyLaptop(laptop);
    renderBankBalance();
    renderSalaryBalance();
}


// ----- Render functions -----
function addLaptopsToSelect(laptops) {
    laptops.forEach(x => addLaptopToSelect(x));
}

function addLaptopToSelect(laptop) {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsSelectElement.appendChild(laptopElement);
}

function renderLaptopSpecs(laptop) {
    laptop["specs"].forEach(x => renderLaptopSpec(x));
}

function renderLaptopSpec(laptopSpec) {
    const laptopSpecElement = document.createElement('li');
    laptopSpecElement.innerText = laptopSpec;
    laptopsSpecsElement.appendChild(laptopSpecElement);
}

function handleLaptopSelectChange(laptop) {
    const selectedLaptop = laptops[laptop.target.selectedIndex];
    laptopsSpecsElement.innerHTML = "";
    selectedLaptop["specs"].forEach(laptopSpec => renderLaptopSpec(laptopSpec));
    renderShowcase(selectedLaptop);
}

function renderShowcase(laptop) {
    showcaseImageElement.src = computersAPI + laptop["image"];
    showcaseTitleElement.innerHTML = laptop["title"];
    showcaseDescriptionElement.innerHTML = laptop["description"];
    showcasePriceElement.innerHTML = `${laptop["price"]} ${currencySign}`;
}

function renderBankBalance() {
    bankBalanceElement.innerText = `${bankBalance} ${currencySign}`;
}
function renderSalaryBalance() {
    salaryBalanceElement.innerText = `${salaryBalance} ${currencySign}`;
}
function renderLoanToPayBalance() {
    loanToPayBalanceElement.innerText = `${loan} ${currencySign}`;
}

function hideLoan() {
    loanBankInfoElement.classList.add("hidden");
    repayLoanButtonElement.classList.add("hidden");
}

function renderLoan() {
    loanBankInfoElement.classList.remove("hidden");
    repayLoanButtonElement.classList.remove("hidden");
}


// ----- Error handling-----
function onMissingImage() {
    this.src = fallbackImageURL;
}


// ----- Fetch api data -----
fetch(computersAPI + "computers")
    .then(response => response.json())
    .then(responseJSON => laptops = responseJSON)
    .then(laptops => addLaptopsToSelect(laptops))
    .then(getFirstLaptop)
    .then(laptop => {
        renderLaptopSpecs(laptop);
        renderShowcase(laptop);
    });


// ----- Initial render -----
renderBankBalance();
renderSalaryBalance();


// ----- Event listeners -----
// Bank
loanButtonElement.addEventListener("click", onGetLoanClick);

// Work
transferButtonElement.addEventListener("click", onTransferMoneyToBankClick);
earnButtonElement.addEventListener("click", onEarnMoneyClick);
repayLoanButtonElement.addEventListener("click", onRepayLoanClick)

// Laptops selection
laptopsSelectElement.addEventListener("change", handleLaptopSelectChange)

// Showcase
showcaseImageElement.addEventListener("error", onMissingImage)
showcaseBuyButtonElement.addEventListener("click", onBuyLaptopClick);