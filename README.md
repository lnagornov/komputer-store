<div align="center">
    <h1>Noroff Assignment 1: JavaScript Fundamentals</h1>
    <img src="https://i.ibb.co/hF09XHG/Unofficial-Java-Script-logo-2-svg.png" width="128" alt="JS">
</div>

[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Komputer Store
It is a dynamic webpage created using “vanilla” JavaScript for Assignment #1 Noroff JS course.

Deployed with heroku [here](https://lnagornov-komputer-store.herokuapp.com).

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This front-end project contains:
* The Bank – an area where you will store funds and make bank loans
* Work – an area to increase your earnings and deposit cash into your bank balance
* Laptops – an area to select and display information about the merchandise
	
## Technologies
Project is created with:
* HTML
* CSS
* "Vanilla" JavaScript
	
## Setup
To run this project, download files and open index.html with a browser.

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Author
Lev Nagornov


---
2022 Made for Noroff JS course
